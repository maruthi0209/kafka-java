package com.okta.javakafka.kafkajava.listener;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.okta.javakafka.kafkajava.pojos.AccountDetails;
import com.okta.javakafka.kafkajava.pojos.Apar;
import com.okta.javakafka.kafkajava.pojos.Category;
import com.okta.javakafka.kafkajava.pojos.Forecast;
import com.okta.javakafka.kafkajava.repository.IForecastRepository;

@Service
public class KafkaConsumer {
	
	@Autowired
	private IForecastRepository iforecastrepository;
	
	@KafkaListener(topics = {"test","two"}, groupId = "mygroup")
	public void listenGroupFoo(String message) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule()).configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Forecast foo = mapper.readValue(message, Forecast.class);
		iforecastrepository.save(foo);
//	    System.out.println("Received Message in group foo: " + foo);
	}
	
	
} 
