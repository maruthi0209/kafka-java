
/** An Entity for defining details about accounts 
 * 
 */

package com.okta.javakafka.kafkajava.pojos;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Defines this pojo as an entity bean and as a table to be created in the
 * database by the name of AccountDetails
 * 
 * @author schennapragada
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "AccountDetails")
public class AccountDetails {

	/**
	 * accountNumber is the primary key for this table it is a generated value
	 */
	@Id
	@GeneratedValue
	@Column(name = "accountid", nullable = false)
	public UUID accountID;
	@Column(name = "account_number", nullable = false, length = 12)
	private String accountNumber;
	private String acHolderName;
	private String accountType;
	private String country;
	private String currency;
	private String createdBy;
	private LocalDate createdTime;
	private String updatedBy;
	private LocalDate updatedTime;

	/**
	 * Creates a many to one mapping between this column and the primary key in
	 * UserDetails table
	 */

	@ManyToOne
	@JoinColumn(name = "userid")
	private UserDetails userID;

	public AccountDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountDetails(UUID accountID, String accountNumber, String acHolderName, String accountType, String country,
			String currency, String createdBy, String updatedBy, UserDetails userID) {
		super();
		this.accountID = accountID;
		this.accountNumber = accountNumber;
		this.acHolderName = acHolderName;
		this.accountType = accountType;
		this.country = country;
		this.currency = currency;
		this.createdBy = createdBy;
//		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
//		this.updatedTime = updatedTime;
		this.userID = userID;
	}

	/**
	 * Setters and Getters for this class
	 * 
	 */

	public UUID getAccountID() {
		return accountID;
	}

	public void setAccountID(UUID accountID) {
		this.accountID = accountID;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAcHolderName() {
		return acHolderName;
	}

	public void setAcHolderName(String acHolderName) {
		this.acHolderName = acHolderName;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDate getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(LocalDate createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LocalDate getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(LocalDate updatedTime) {
		this.updatedTime = updatedTime;
	}

	public UserDetails getUserID() {
		return userID;
	}

	public void setUserID(UserDetails userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "AccountDetails [accountID=" + accountID + ", accountNumber=" + accountNumber + ", acHolderName="
				+ acHolderName + ", accountType=" + accountType + ", country=" + country + ", currency=" + currency
				+ ", createdBy=" + createdBy + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy
				+ ", updatedTime=" + updatedTime + ", userID=" + userID + "]";
	}
	
	
}
