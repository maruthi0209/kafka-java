package com.okta.javakafka.kafkajava.controller;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.okta.javakafka.kafkajava.pojos.AccountDetails;
import com.okta.javakafka.kafkajava.pojos.Forecast;
import com.okta.javakafka.kafkajava.pojos.UserDetails;
import com.okta.javakafka.kafkajava.pojos.UserRole;


@RestController
public class KafkaController {
		
	
}
