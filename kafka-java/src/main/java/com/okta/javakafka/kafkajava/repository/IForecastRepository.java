package com.okta.javakafka.kafkajava.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.okta.javakafka.kafkajava.pojos.Forecast;

@Repository
public interface IForecastRepository extends CrudRepository<Forecast, UUID> {

}
